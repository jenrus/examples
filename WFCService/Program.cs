﻿using System;
using System.Data;
using System.ServiceModel;
using System.Runtime.Serialization;


namespace PrinterService
{
    [ServiceContract]
    public interface IPrinter
    {
        [OperationContract]
        SendingData PrintString(string str);
    }

    [DataContract]
    public class SendingData
    {
        [DataMember]
        public int num, sum;
        public SendingData(int num, int sum)
        {
            this.num = num;
            this.sum = num;
        }
    }

    public class Printer : IPrinter
    {     

        public SendingData PrintString(string str)
        {
            SendingData data = new SendingData(0, 0); 
            Console.WriteLine(str);
            foreach (char chr in str)
            {
                data.num++;
                data.sum += (int)chr;
            }
            return data;
        }

    }
}

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceHost host = new ServiceHost(typeof(PrinterService.Printer));
            BasicHttpBinding binding = new BasicHttpBinding();
            host.AddServiceEndpoint(typeof(PrinterService.IPrinter), binding, "http://localhost:2569/");
            host.Open();
            Console.WriteLine("Server is up and running");
            Console.WriteLine("Press any key to terminate");
            Console.ReadKey();
            host.Close();
        }
    }
}