package javatowcf;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class JavaToWCF
{
    public static void main(String[] args)
    {
        try
        {
            String str;
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter String");
            str = br.readLine();
            
            SendingData received = printString(str);
            System.out.println("Number of chars in string is " + received.getNum());
            System.out.println("Sum of chars' ASCII codes is " + received.getSum());
            
        } catch (Exception e1)
        {
            System.out.println(e1.getMessage());
        }
    }   
}
